= OpenAPI作業用リポジトリテンプレートプロジェクト

== 必要ソフトウェア

https://www.docker.com/[docker, docker-compose]

== 使用しているソフトウェア

https://github.com/Redocly/redoc[ReDoc]

== ブランチ運用ルール

* masterブランチが変更の主流となる
* 変更時はfeatureブランチを作成して、masterブランチへマージする
* featureブランチの名前はgitlabの自動生成に従うものとする


== 使い方

1. 本プロジェクトをローカルに落としてくる
2. openapi/openapi.ymlの内容を編集(編集用ツール : https://editor.swagger.io/[swagger editor] , https://marketplace.visualstudio.com/items?itemName=42Crunch.vscode-openapi[vscodeの拡張] 等)
3. `docker-compose up -d` を実行
4. `http://localhost:30824` にアクセス
